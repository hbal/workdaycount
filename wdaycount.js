
// Weekdays in Turkish
const weekdays=["Pazar","Pazartesi","Salı","Çarşamba","Perşembe","Cuma","Cumartesi"]

// Converts 23.12.2019 to ISO format 2019-12-23 
const fromTrToISODate = (x) =>
  new Date(x.trim().split(".").reverse().join("-")).toISOString().split("T")[0];

// Shows the weekday of the origin's value in the target in Turkish
function showday(origin,target){
  target.innerText=weekdays[(new Date(origin.value)).getDay()]
  return false
}

// Gets the names and times of semesters from times.txt as an array of
// [Semester_name,[Semester_Start,Semester_End]]
// Semester_Start and Semester_End are ISO String dates
async function getTimes() {
  let response = await fetch("times.txt");
  let text = await response.text();
  //console.log("getTimes received: \n"+text)
  let times = text.split(/\r?\n/).map((line) => line.split(","));
  //console.log("parsed "+times.length+" lines.")
  times = times
    .filter((x) => x[0].length > 0)
    .map((x) => [x[1], x[0].split("-").map((x) => fromTrToISODate(x))]);
  return times;
}

// Gets only the dates of holidays from holidays.txt as an array of 
// Date getTime results.
async function getHolidays() {
  let response = await fetch("holidays.txt");
  let text = await response.text();
  let holidays = text.split(/\r?\n/).map((line) => line.split(","));
  holidays = holidays
    .filter((x) => x[0].length > 0)
    .map((x) => (new Date(fromTrToISODate(x[0]))).getTime());
  return holidays;
}

// Returns an array of 7 td elements with checkbox input with ids; 
// i0, i2, ... ,i6
// i is given as input
function getCheckboxes(i) {
  ret = [];
  for (let j of Array.from(Array(7).keys())) {
    let td = document.createElement("td");
    let inp = document.createElement("input");
    inp.type = "checkbox";
    inp.id = i.toString() & j.toString();
    td.appendChild(inp);
    ret.push(td);
  }
  return ret;
}

// Helper function for getting semester times for the selected dates
async function fillTimesWrap(){
  await fillTimes(stdate.value,enddate.value);
}

// Helper function for getting semester times for the selected dates
async function fillTimesWrap2(){
  await fillTimes(stdate2.value,null);
}

// Filters and pushes the overlapping semester times between s start date and e end date 
// to a table with "outtable" id and adds the table to the outtablediv element
// s and e should be JS date object
async function fillTimes(st, en) {
  tms = await getTimes(); // read all the semester times
  //console.log("Received "+tms.length+" semester times from times.txt")
  let tbl = document.createElement("table"); // Prepare table
  tbl.id = "outtable";
  let tr0 = document.createElement("tr"); // header row
  tr0.innerHTML =
    "<td>Tarihler</td><td>Dönem</td><td>Pzt</td><td>Sal</td><td>Çrş</td><td>Prş</td><td>Cum</td><td>Cts</td><td>Pzr</td></tr>";
  tbl.appendChild(tr0);
  tr1 = document.createElement("tr"); // the row for default; the complementing dates of semester intervals
  let td1 = document.createElement("td");
  td1.innerText = "Diğer günler";
  tr1.appendChild(td1);
  let td2 = document.createElement("td");
  tr1.appendChild(td2);
  for (let e of getCheckboxes(0)) tr1.appendChild(e); // adding checkboxes
  tbl.appendChild(tr1);
  for (let t of tms) { // adding semester times
    //console.log("Processing "+t[0]+" "+t[1][0]+" "+t[1][1])
    let i = 1;
    if ((!en || t[1][0] <= en) && t[1][1] >= st) { // if there is an overlap of (st,en) and current semester start and end dates
      //console.log("Putting "+t[0]+" "+t[1][0]+" "+t[1][1])
      let tr = document.createElement("tr"); // add it to the table
      let td3 = document.createElement("td");
      td3.innerHTML =
        "<div id='s" +
        i +
        "'>" +
        t[1][0] +
        "</div> <br/> <div id='e" +
        i +
        "'>" +
        t[1][1] +
        "</div>";
      tr.appendChild(td3);
      i += 1;
      let td4 = document.createElement("td");
      td4.innerText = t[0];
      tr.appendChild(td4);
      for (let j of getCheckboxes(i)) tr.appendChild(j);
      tbl.append(tr);
    }
  }
  document.getElementById("outtablediv").innerHTML = "";
  document.getElementById("outtablediv").appendChild(tbl);
}

// wrapper function for computeWorkdays
function computeWorkdaysWrap(){
  if(outtable.rows.length==0) return;
  computeWorkdays(stdate.value,enddate.value)
}

// computes the workdays using the stdate and enddate inputs, the marked days within the semester
// the holidays.txt files.
// writes the number of workdays in the workdays inner text, and list of these workdays in the
// outdays object in the page.
async function computeWorkdays(sd,ed) {
  holidays = await getHolidays(); // read holidays
  periods = await getSemesters();
  dt0 = new Date(sd);
  dt1 = new Date(ed);
  arrworkdays=[];
  ndays = 0;
  curdate = dt0;
  while (curdate <= dt1) { // starting from first day, until the last day entered
    if(checkdate(curdate,holidays,periods)){
      ndays += 1;
      arrworkdays.push(curdate.toLocaleDateString()+" "+weekdays[curdate.getDay()])
    }
    curdate.setDate(curdate.getDate() + 1); // move to next day
  }
  document.getElementById("workdays").innerText=ndays; // populate the results in the page
  document.getElementById("outdays").innerHTML="<br/>Günler:<br/>"+arrworkdays.join("<br/>")+"<br/>"
}

// wrapper function for computeWorkdays
function computeEnddateWrap(){
  if(outtable.rows.length==0) return;
  computeEnddate(stdate2.value,workdays2.value);
}

// computes the enddate using the stdate and workday inputs, the marked days within the semester
// the holidays.txt files.
// writes the end date as enddate2 inner text, and list of these workdays in the
// outdays object in the page.
async function computeEnddate(sd,wdays) {
  let holidays = await getHolidays(); // read holidays
  let periods = getSemesters();
  let dt0 = new Date(sd);
  let ndays = 0;
  let arrworkdays=[];
  curdate = dt0;
  while (ndays<wdays) { // starting from first day, until the last day entered
    if(checkdate(curdate,holidays,periods)){
      ndays += 1;
      arrworkdays.push(curdate.toLocaleDateString()+" "+weekdays[curdate.getDay()])
    }
    curdate.setDate(curdate.getDate() + 1); // move to next day
  }
  curdate.setDate(curdate.getDate() - 1);
  document.getElementById("enddate2").innerText=curdate.toLocaleString().split(" ")[0]; // populate the results in the page
  document.getElementById("outdays").innerHTML="<br/>Günler:<br/>"+arrworkdays.join("<br/>")+"<br/>"
}

function getSemesters(){
  let nrows = outtable.rows.length; // read the semester time weekdays from the page
  let rows = outtable.rows;
  let periods = [];
  let arrworkdays=[]
  for (let i = 1; i < nrows; i++) { // for each row (semester time)
    tmp = [];
    for (let j = 0; j < 7; j++) { // push the checked days into a tmp array
      tmp.push(rows[i].cells[j + 2].getElementsByTagName("input")[0].checked);
    }
    sd = ed = null;
    if (i > 1) { // for semester times, other than the complementary, read the dates
      tmps = rows[i].cells[0].getElementsByTagName("div")[0].innerText;
      tmpe = rows[i].cells[0].getElementsByTagName("div")[1].innerText;
      sd = new Date(tmps);
      ed = new Date(tmpe);
    } 
    periods.push([sd, ed, tmp]); // and add start time, end time and day check statuses to an array
  }
  return periods;
}

function checkdate(dt,holidays,periods){
  if (holidays.includes(dt.getTime())) return false; // if the current date is a holiday, skip that day
  let found=false;
  for (let i = 1; i < periods.length; i++) { // for each semester time
    if (!found && curdate >= periods[i][0] && curdate <= periods[i][1]) { // if date overlaps with the semester time
      if (periods[i][2][(curdate.getDay() + 6) % 7]) { // and the day is checked (available to work), return true
        return true;
      }
      return false; // if not available to work, return false
    }
  }
  if (periods[0][2][(curdate.getDay() + 6) % 7])  // if we could not find this day in the semester times
    return true; // and it is marked as checked (available to work) in the complementary semester times
  return false; // else return false
}

function openPart(evt,el) {
  let x = document.getElementsByClassName("method");
  for (let i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  let tablinks = document.getElementsByClassName("tablink");
  for (let i = 0; i < x.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" red", "");
  }
  el.style.display = "block";
  evt.currentTarget.className += " red";
}

document.getElementById("fromdays").style.display="none";